﻿using NUnit.Framework;
using System;
using PerformUnitTesting;

namespace PerformUnitTestingTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void CreateGame()
        {
            
        }

        void rollMany(int roll, int pins)
        {
            for (int i = 0; i < 20; i++)
                game.Roll(pins);
        }

        [Test]
        public void GutterGame()
        {
            for (int i = 0; i < 20; i++)
                game.Roll(0);

            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void OnePinGame()
        {
            for (int i = 0; i < 20; i++)
                game.Roll(1);

            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test]
        public void OneSpareFirstFrame()
        {
            for (int i = 0; i < 20; i++)
                game.Roll(9); game.Roll(1);
                rollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(29));
        }

    }
}
